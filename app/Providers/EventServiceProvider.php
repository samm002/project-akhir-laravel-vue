<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\UserRegisterEvent;
use App\Events\GenerateOtpCodeEvent;
use App\Events\UserVerifiedEvent;
use App\Listeners\SendMailUserListener;
use App\Listeners\GenerateOtpCodeListener;
use App\Listeners\UserVerifiedListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserRegisterEvent::class => [
            SendMailUserListener::class
        ],
        GenerateOtpCodeEvent::class => [
            GenerateOtpCodeListener::class
        ],
        UserVerifiedEvent::class => [
            UserVerifiedListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
