<?php

namespace App\Listeners;

use App\Events\GenerateOtpCodeEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\GenerateOtpCodeMail;

class GenerateOtpCodeListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\GenerateOtpCodeEvent  $event
     * @return void
     */
    public function handle(GenerateOtpCodeEvent $event)
    {
        Mail::to($event->user)->send(new GenerateOtpCodeMail($event->user));
    }
}
