<?php

namespace App\Listeners;

use App\Events\UserVerifiedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;
use App\Mail\verifiedUserMail;

class UserVerifiedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UserVerifiedEvent  $event
     * @return void
     */
    public function handle(UserVerifiedEvent $event)
    {
        Mail::to($event->user)->send(new VerifiedUserMail($event->user));
    }
}
