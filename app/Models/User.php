<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, HasUuids ,Notifiable;

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    // Generate role secara otomatis ketika user registrasi (ketika data user dibuat)
    public static function boot() 
    {
        parent::boot();

        static::creating(function ($model) {
            $model->role_id = $model->getRoleUser();
        });

        static::created(function ($model) {
            $model->generateOtpCodeUser();
        });
    }

    // Assingn role 'user' sebagai default
    public function getRoleUser()
    {
        $role = Role::where('name', 'user')->first();

        return $role->id;
    }

    // Buat fungsi untuk mendapatkan user yang memiliki role admin :
    public function isAdmin()
    {
        if ($this->role) {
            if ($this->role->name == 'admin') {
                return true;
            }
        }
    }

    // Generate 6 angka random sebagai kode otp
    public function generateOtpCodeUser() {
        $min = 000000;
        $max = 999999;
        do {
            $randomNumber = mt_rand($min, $max);
            $check = OtpCode::where('otp', $randomNumber)->first();
        } while ($check);

        $now = Carbon::now();

        // Create or Update (jika data belum ada)
        $otp_code = OtpCode::updateOrCreate(
            ['user_id' => $this->id],
            [
                'otp' => $randomNumber,
                'valid_until' => $now->addMinutes(5)
            ]
            );
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
        'photo_profile',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() 
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function otpCode()
    {
        return $this->hasOne(OtpCode::class, 'user_id');
    }
}
