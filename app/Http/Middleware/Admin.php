<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Role;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();

        /* Logic lama dari tugas 8
        $adminRoleId = Role::where('name', 'admin')->value('id');

        if ($user->role_id === $adminRoleId) {
            return $next($request);
        }
        */

        // Logic baru dari pembahasan tugas 8
        if ($user->isAdmin()) {
            return $next($request);
        }

        return response()->json([
            'response_code' => '01',
            'response_message' => 'Akses terbatas, hanya untuk admin'
        ], 401);
    }
}
