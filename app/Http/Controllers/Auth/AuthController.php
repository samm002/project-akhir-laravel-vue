<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\OtpCode;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Events\UserRegisterEvent;
use App\Events\GenerateOtpCodeEvent;
use App\Events\UserVerifiedEvent;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function register(Request $request) 
    {
        $request->validate([
            'email' => 'required|unique:users,email|email',
            'name' => 'required',
            'password' => 'required|confirmed|min:8'
        ]);

        $user = User::create([
            'email' => $request->input('email'),
            'name' => $request->input('name'),
            'password' => Hash::make($request->input('password'))
        ]);

        $data['user'] = $user;

        // Test email (Sekarang pindah ke listener)
        // Mail::to($user->email)->send(new UserRegisterMail($user));

        event(new UserRegisterEvent($user));

        return response()->json([
        'response_code' => '00',
        'response_message' => 'user berhasil di register',
        'data' => $data,
        ], 201);
    }


    public function login(Request $request) 
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|confirmed|min:8'
        ]);

        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            $user = User::where('email', $request->email)->first();
            // Jika tidak ada email yang ditemukan
            if (!$user) {
                return response()->json(['error' => 'Unauthorized, No email found'], 401);
            }
            // Jika email ditemukan, tetapi password tidak sesuai
            if (!Hash::check($request->password, $user->password)) {
                return response()->json(['error' => 'Unauthorized, Password didn\'t match'], 401);
            }
            // Response default tanpa kondisi cek email dan password
            // return response()->json(['error' => 'Unauthorized'], 401);
        }

        $data['token'] = $token;

        return response()->json([
        'response_code' => '00',
        'response_message' => 'user berhasil login',
        'data' => $data,
        ], 200);
    }

    public function update_password(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|confirmed|min:8'
        ]);

        $user = auth()->user();

        $input_email = $request->input('email');

        if ($user->email !== $input_email) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'User dengan email ' . $input_email . ' tidak ditemukan',
            ], 404);
        }

        $user->password = Hash::make($request->input('password'));

        $user->save();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'password berhasil di update'
        ], 200);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Logout berhasil']);
    }

    public function generateOtpCode(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $user = User::where('email', $request->email)->first();

        // Panggil fungsi untuk generate otp code yang telah dibuat di model
        $user->generateOtpCodeUser();

        $data['user'] = $user;

        event(new GenerateOtpCodeEvent($user));

        return response()->json([
            // 'response_code' => '00',
            'success' => true,
            'response_message' => 'OTP Code Berhasil di generate'
        ], 200);
    }

    public function emailVerification(Request $request)
    {
        $request->validate([
            'otp' => 'required|numeric|min:6'
        ]);

        // Mencari OTP code pada database
        $otp_code = OtpCode::where('otp', $request->otp)->first();

        // Jika kode OTP tidak ditemukan / bernilai salah
        if (!$otp_code) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'OTP Code tidak ditemukan'
            ], 400); 
        }

        // Menggunakan library carbon untuk mengambil waktu sekarang
        $now = Carbon::now();

        // Jika kode OTP sudah melewati masa aktif selama 5 menit (sudah tidak berlaku)
        if ($now > $otp_code->valid_until) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'otp code sudah tidak berlaku, silahkan generate ulang'
            ], 400); 
        }

        // Update bagian email_verified_at pada user
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = $now;

        $user->save();

        
        // Setelah update email_verified berhasil, hapus variabel otp_code
        $otp_code->delete();
        
        event(new UserVerifiedEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'email sudah terverifikasi'
        ], 200); 

    }

    // Function untuk dapat detail token
    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
    }
}
