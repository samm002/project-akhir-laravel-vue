<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Campaign;
use Illuminate\Support\Facades\File;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['campaign'] = Campaign::all();

        if (count($data['campaign']) === 0) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Gagal menampilkan data campaign, tidak ada data campaign untuk ditampilkan',
            ], 200);
        }
        return response()->json([
            'response_code' => '00',
            'response_message' => 'tampil data berhasil',
            'data' =>  $data,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'image' => 'nullable | mimes:png,jpg,jpeg,webp,svg,jfif',
            'required' => 'nullable | numeric',
            'collected' => 'nullable',
        ]);

        $campaign = new Campaign;

        if($request->has('image')) {
            // logic untuk penamaan file : title_tanggal_waktu
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $imageFolder = 'photo/campaign/';
            $imageName = Time() . '.' . $extension;
            $imageLocation = $imageFolder . $imageName;

            try {
                $request->image->move(public_path($imageFolder), $imageName);
                $campaign->image = $imageLocation;
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => $th->getMessage()
                ], 400);
            }
        }

        $campaign->title = $request->input('title');
        $campaign->description = $request->input('description');
        $campaign->address = $request->input('address');
        $campaign->collected = $request->input('collected', 0);
        $campaign->required = $request->input('required');

        $campaign->save();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Tambah Campaign berhasil',
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign = Campaign::find($id);

        if (!$campaign) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Detail Data Campaign Dengan ID tersebut tidak ditemukan',
            ], 404);
        }

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Detail Data Campaign',
            'data' => $campaign,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'address' => 'required',
            'image' => 'nullable | mimes:png,jpg,jpeg,webp,svg,jfif',
            'collected' => 'nullable | numeric',
            'required' => 'required | numeric',
        ]);

        $campaign = Campaign::find($id);

        if (!$campaign) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Detail Data Campaign Dengan ID Tersebut Tidak Ditemukan',
            ], 404);
        }

        if($request->has('image')) {
            // Hapus image lama (apabila ingin mengganti image)
            File::delete($campaign->image);

            // logic untuk penamaan file : title_tanggal_waktu
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $imageName = Time() . '.' . $extension;
            $imageFolder = 'photo/campaign/';
            $imageLocation = $imageFolder . $imageName;

            try {
                $request->image->move(public_path($imageFolder), $imageName);
                $campaign->image = $imageLocation;
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => $th->getMessage()
                ], 400);
            }
        }

        $campaign->title = $request->input('title');
        $campaign->description = $request->input('description');
        $campaign->address = $request->input('address');
        $campaign->collected = $request->input('collected', $campaign->collected);
        $campaign->required = $request->input('required');

        $campaign->save();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Update Campaign berhasil'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);

        if (!$campaign) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Detail Data Campaign Dengan ID Tersebut Tidak Ditemukan',
            ], 404);
        }
    
        File::delete($campaign->image);

        $campaign->delete();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'berhasil Menghapus Campaign'
        ], 200);        
    }
}
