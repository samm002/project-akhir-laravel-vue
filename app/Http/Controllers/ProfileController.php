<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    public function get_profile()
    {
        $data_user = auth()->user();
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil ditampilkan',
            'data' => $data_user,
            ], 200);
    }

    public function update_profile(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'photo_profile' => 'nullable|mimes:jpg,jpeg,png,webp,svg'
        ]);

        // Mencari user berdasarkan yang telah login dengan memasukkan token pada request header
        $user = auth()->user();

        if($request->has('photo_profile')) {
            // Hapus image lama (apabila ingin mengganti image)
            File::delete($user->photo_profile);

            // logic untuk penamaan file : title_tanggal_waktu
            $image = $request->file('photo_profile');
            $extension = $image->getClientOriginalExtension();
            $imageName = Time() . '.' . $extension;
            $imageFolder = 'photo/user/';
            $imageLocation = $imageFolder . $imageName;

            try {
                $request->photo_profile->move(public_path($imageFolder), $imageName);
                $user->photo_profile = $imageLocation;
            } catch (\Throwable $th) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => $th->getMessage()
                ], 400);
            }
        }

        $user->name = $request->input('name');

        $user->save();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil diupdate'
        ], 200);
    }
}
