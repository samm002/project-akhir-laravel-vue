<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crowd Funding</title>
    @vite('resources/sass/app.scss')
    @vite('resources/js/app.js')
</head>
<body>
    <div id="app">
        <example-component></example-component>
    </div>
</body>
</html>