import { createRouter, createWebHistory } from "vue-router"
import Home from './views/Home.vue'
import About from './views/About.vue'
import Campaign from './views/Campaign.vue'

const routes = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/campaign',
            name: 'Campaign',
            component: Campaign,
            meta: { requiresAuth: true },
        },
        {
            path: '/:catchAll(.*)',
            redirect: {name: 'Home'},
        },
    ]
})

routes.beforeEach((to, from) => {
    let user = {
        login: true,
        role: 'user'
    }

    if(to.meta.requiresAuth) {
        if(!user.login) {
            if(user.role != "admin") {
                console.log(user)
                alert('Hanya Admin yang sudah login yang boleh mengakses halaman')
                return {
                    path: '/'
                }
            }
        }
    }
})

export default routes