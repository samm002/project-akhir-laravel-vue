<?php

use App\Http\Controllers\CampaignController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ProfileController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    //     return $request->user();
    // });
    
Route::group([
    'prefix' => 'auth'
], function() {
    route::post('register', [AuthController::class, 'register']);
    route::post('login', [AuthController::class, 'login']);
    route::post('logout', [AuthController::class, 'logout'])->middleware('email_verified');
    route::post('update-password', [AuthController::class, 'update_password'])->middleware('email_verified');
    route::post('generate-otp-code', [AuthController::class, 'generateOtpCode'])->middleware('auth');
    route::post('verification-email', [AuthController::class, 'emailVerification'])->middleware('auth');
});

// Route baru untuk campaign pada tugas form ajax
Route::apiResource('/campaign', CampaignController::class);

Route::group([
    'middleware' => ['api', 'auth', 'email_verified']
], function() {
    Route::apiResource('/role', RoleController::class)->middleware('is_admin')->except(['index, show']);
    // Sementara dinonaktifkan untuk keperluan tugas form ajax
    // Route::apiResource('/campaign', CampaignController::class)->middleware('is_admin')->except(['index, show']);
    route::get('get-profile', [profileController::class, 'get_profile']);
    route::post('update-profile', [profileController::class, 'update_profile']);
});